const { BigQuery } = require('@google-cloud/bigquery')

const projectId = process.env.GCLOUD_PROJECT
const bqClient = new BigQuery({ projectId })
const BQ_DATASET = 'ota'
const BQ_TABLE = 'firmwares'

exports.updateRelease = async (req, res) => {

    const { version } = req.query
    const [datasets] = await bqClient.getDatasets()
    let dataset = datasets.find(dataset => dataset.id === BQ_DATASET)

    const [tables] = await dataset.getTables()
    let table = tables.find(table => table.id === BQ_TABLE)
    let message = '';
    //console.log('Tables', tables, table)

    try {
        //var d = new Date();
        //dateTime = d.getUTCFullYear() + "-" + d.getUTCMonth() + "-" + d.getUTCDate() + " " + d.getUTCHours() + ":" + d.getUTCMinutes() + ":" + d.getUTCSeconds() + ":" + d.getUTCMilliseconds()
        //timestamp = (new Date()).getTime() / 100,000;

        const timestamp = bqClient.timestamp(new Date());
        data = await getReleaseByVersion(version);
        const row = {
            id: data.id,
            bucket: data.bucket,
            version: data.version,
            fullname: data.fullname,
            filename: data.filename,
            variant: data.variant,
            createdAt: timestamp,
            latestVersion: data.latestVersion,
            env: 'prod',
            eventType: data.eventType,
        }

        const res = await table.insert(row, { ignoreUnknownValues: true });
        console.log('Updated', res)
        message = "New version released to PROD : " + version;
    } catch (e) {
        console.log('Update Failed', e, JSON.stringify(e))
        message = 'Something wrong';
    }


    res.status(200).send(message);
};

async function getReleaseByVersion(version) {
    try {
        console.log('-- Get data for version : ', version)

        const queryParams = {
            query: `
        SELECT 
          *
        FROM \`divolgo-jenkins.ota.firmwares\`
        where version = @version
        order by createdAt desc
        limit 1
      `,
            params: {
                version
            }
        }

        const [rows] = await bqClient.dataset(BQ_DATASET).table(BQ_TABLE).query(queryParams)

        if (rows.length > 0) {
            data = rows[0]
            console.log("Data selected : ", data.fullname);
            return data;
        } else {
            console.log("Invalid version");
            return null
        }

    } catch (err) {
        console.log("Error : ", err.message);
        res.status(500).send(err.message);
    }
}
