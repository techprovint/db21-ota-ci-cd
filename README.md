# Serverless Continuous Integration and OTA update flow using Google Cloud Build and Arduino ESP32

How to build your firmware continuously in the cloud and sent to your devices automatically.

### Setup Google cloud tools and project

* Install beta components:
    * `gcloud components install beta`
* Authenticate with Google Cloud:
    * `gcloud auth login`
* Create cloud project — choose your unique project name:
    * `gcloud projects create YOUR_PROJECT_NAME`
* Set current project
    * `gcloud config set project YOUR_PROJECT_NAME`

* Cloud Build Setup :
  * Open the Cloud Build page.
  * Enable Cloud Build API if not enabled yet.
  * Select the Triggers tab.
  * Click on Create Trigger.
  * Now you can select your Git repository and authenticate with your provider. I used Bitbucket.
  * On the last step, Trigger Settings, let’s create a trigger by git tags.
  * Name it — “Trigger by Build Tag”.
  * Select Tag as the Trigger type.
  * Select cloudbuild.yaml as the Build configuration type.
  * Click on Create Trigger.

* Cloud Storage Setup :
  * Now Open the Cloud Storage page
  * Select the Browser Tab.
  * Click on Create bucket.
  * Create a bucket with the name YOUR_PROJECT_NAME-firmware and select the let the other values as the defaults. In my case, the name of the bucket was ota-divolgo-gcp-320311-firmware.
  * Here the PROJECT_ID should be used for YOUR_PROJECT_NAME
  * Click on Create.


* Permissions
  * allUsers, allAuthenticatedUsers were given the access to storage bucket created ( Storage Legacy Bucket Owner, Storage Legacy Bucket Reader, Storage Legacy Object Owner, Storage Legacy Object Reader  

### Deploy Cloud Functions

* Run on the command line:
```
Navigate to updateRelease folder and run 
./deploy-update-release.sh

./deploy-prod.sh
Navigate to updateRelease folder and run 
./deploy-update-release.sh

```

### Update CLOUD_FUNCTION_URL 

  * Set CLOUD_FUNCTION_URL on main.cpp to  `https://us-central1-divolgo-jenkins.cloudfunctions.net/getDownloadUrl`

### Upload firmware with PlatfomIO

“Build” and “Upload” buttons on PlatformIO Toolbar on VS Code.

## How to release
```
- Update version on platformio.ini first
- Commit the files
git add src/main.cpp platformio.ini
git commit -m "[feat] Add blink feature"

- Tag this commit
git tag -a vMAJOR.MINOR.PATCH -m "Add blink feature"

- Push to the repository with the tags
git push -u origin master --tags

```
## Promoting a dev version to PROD

```
Enter the dev version you want to promote below.

https://us-central1-divolgo-jenkins.cloudfunctions.net/updateRelease?version=vx.y.z

```

### References

* https://dzone.com/articles/how-to-approach-ota-updates-for-iot
* https://cloud.google.com/functions/docs/writing
* https://cloud.google.com/functions/docs/tutorials/storage
* http://docs.platformio.org/en/latest
* https://medium.com/google-cloud/serverless-continuous-integration-and-ota-update-flow-using-google-cloud-build-and-arduino-d5e1cda504bf