#ifndef UTILS_H
#define UTILS_H

#include "config.h"
#include "debug.h"



void gpio_init() {
	digitalWrite(EPD_RST,   HIGH);
    pinMode(EPD_RST,    INPUT);
    digitalWrite(EPD_PWR_EN, LOW);
    pinMode(EPD_PWR_EN, 	OUTPUT);
    
    digitalWrite(LED_EN, 	HIGH);
	digitalWrite(SOFT_PWR_OFF, HIGH);

	pinMode(ON_BTN_SIG, 	INPUT);
	pinMode(BC_CHG_SIG, 	INPUT);
	pinMode(BC_PGOOD, 		INPUT);
	pinMode(USB_DETECT_SIG, 	INPUT);

	pinMode(LED_EN, 		OUTPUT);
    digitalWrite(LED_EN, 	HIGH);
	pinMode(LED_INT, 		INPUT);
	pinMode(SOFT_PWR_OFF, 	OUTPUT);

	pinMode(RTC_INT, 		INPUT);
	pinMode(BM_ALARM, 		INPUT);
    
	pinMode(TEMP_ALERT_1, 	INPUT);
	pinMode(TEMP_ALERT_2, 	INPUT);

    pinMode(SPI_CS, 		OUTPUT);
	digitalWrite(SPI_CS, 	LOW);

	delay_ms(20);
}

void epd_gpio_init(){
	// digitalWrite(EPD_PWR_EN, LOW);
	digitalWrite(EPD_RST, 	LOW);
	pinMode(EPD_PWR_EN, 	OUTPUT);
	pinMode(EPD_RST, 		OUTPUT);
	pinMode(EPD_BUSY_N, 	INPUT_PULLUP);
	
	delay_ms(20);
}

void HBZ9_3V3_enable(){
	digitalWrite(EPD_PWR_EN, HIGH);
	delay_ms(10);
    digitalWrite(EPD_RST, 	HIGH);
    delay_ms(10);
}

void HBZ9_3V3_desable(){
	digitalWrite(EPD_PWR_EN, LOW);
	delay_ms(10);
    digitalWrite(EPD_RST, 	LOW);
    delay_ms(10);
}

// Range : low/medium/high
void cpu_freq_set(String range) 
{
    delay_ms(100);
    if(range == "low") {
        rtc_clk_cpu_freq_set(RTC_CPU_FREQ_80M);
    } else if (range == "medium") {
        rtc_clk_cpu_freq_set(RTC_CPU_FREQ_160M);
    } else {
        rtc_clk_cpu_freq_set(RTC_CPU_FREQ_240M);
    }
    delay_ms(100);
}

void power_off() {
    digitalWrite(LED_EN, HIGH);
    Wire.begin(I2C_SDA, I2C_SCL, 100000);
    led_blinking_poweroff();
    log_message(DEBUG_MSG, "Powering off");
    delay_ms(200);
    pinMode(SOFT_PWR_OFF, OUTPUT);
    digitalWrite(SOFT_PWR_OFF, LOW);
    delay_ms(100);
}

void power_off_check() {
    
    if(interruptCounter) {
        interruptCounter = 0;
        log_message(DEBUG_MSG, "Power Button press detected.!");
        if(!digitalRead(USB_DETECT_SIG)) {
            log_message(DEBUG_MSG, "Power off by user..!");

            save_battery_log(battery_voltage, battery_capacity, "PBR");  // PBR: Power butten pressed while running

            battery_status.remove(0);
            battery_status += "OFF";
            // save_battery_log(battery_voltage, battery_capacity);
            
            timerWrite(timer_w, 0); //reset timer (feed watchdog)

            if (read_int_value_from_eeprom(EEPROM_MEM_ADDR_DEVELOPER_MODE)) {
                write_value_to_eeprom(EEPROM_MEM_ADDR_DEVELOPER_MODE, "0");
                log_message(DEBUG_MSG, "Developer mode turned off");
            }

            delay_ms(100);
            power_off();
        } else{
            write_value_to_eeprom(EEPROM_MEM_ADDR_DEVELOPER_MODE, "0");
            log_message(DEBUG_MSG, "Developer mode turned off");
            log_message(DEBUG_MSG, "Device is going to restart");
            delay_ms(100);
            ESP.restart();
        }  
    } 
}

#endif /* UTILS_H */
