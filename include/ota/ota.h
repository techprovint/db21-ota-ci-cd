#define CLOUD_FUNCTION_URL "https://us-central1-divolgo-jenkins.cloudfunctions.net/getDownloadUrl" //Techprovint

String getDevId()
{
    byte mac[6];
    WiFi.macAddress(mac);
    // USE_SERIAL.println(devId);
    return String(mac[5], HEX) + String(mac[4], HEX) + String(mac[3], HEX) + String(mac[2], HEX) + String(mac[1], HEX) + String(mac[0], HEX);
}

String getDownloadUrl()
{
    HTTPClient http;
    String downloadUrl;
    log_message(DEBUG_OTA, "[HTTP] begin...");

    timerWrite(timer_w, 0); //reset timer (feed watchdog)

    String url = CLOUD_FUNCTION_URL;
    url += String("?version=v") + CURRENT_VERSION;
    url += String("&variant=") + VARIANT;
    url += String("&devid=") + getDevId();

    log_message(DEBUG_OTA, "url:" + url);
    
    http.begin(url);

    timerWrite(timer_w, 0); //reset timer (feed watchdog)

    int httpCode = http.GET(); // start connection and send HTTP header > httpCode will be negative on error

    timerWrite(timer_w, 0); //reset timer (feed watchdog)
    
    if (httpCode > 0)
    {
        // HTTP header has been send and Server response header has been handled
        log_message(DEBUG_OTA, "[HTTP] GET... code: " + String(httpCode));

        timerWrite(timer_w, 0); //reset timer (feed watchdog)

        if (httpCode == HTTP_CODE_OK)
        { // file found at server
            String payload = http.getString();
            log_message(DEBUG_OTA, "payload: " + payload);
            downloadUrl = payload;
        }
        else
        {
            log_message(DEBUG_OTA, "Device is up to date!");
        }
    }
    else
    {
        log_message(DEBUG_ERROR, "Connect to Server Failed.!");
        led_blinking_ota_update_fail();
    }

    timerWrite(timer_w, 0); //reset timer (feed watchdog)

    http.end();

    timerWrite(timer_w, 0); //reset timer (feed watchdog)

    return downloadUrl;
}

/* 
 * Download binary image and use Update library to update the device.
 */
bool downloadUpdate(String url)
{
    HTTPClient http;
    // led_blinking_ota_update_receive();

    log_message(DEBUG_OTA, "[HTTP] Download begin");

    timerWrite(timer_w, 0); //reset timer (feed watchdog)

    http.begin(url);

    timerWrite(timer_w, 0); //reset timer (feed watchdog)
    // start connection and send HTTP header
    int httpCode = http.GET();

    timerWrite(timer_w, 0); //reset timer (feed watchdog)

    if (httpCode > 0)
    {
        // HTTP header has been send and Server response header has been handled
        log_message(DEBUG_OTA, "[HTTP] GET... code: " + String(httpCode));

        // file found at server
        if (httpCode == HTTP_CODE_OK)
        {

            int contentLength = http.getSize();
            log_message(DEBUG_OTA, "contentLength : " + String(contentLength));

            timerWrite(timer_w, 0); //reset timer (feed watchdog)

            if (contentLength > 0)
            {
                bool canBegin = Update.begin(contentLength);

                timerWrite(timer_w, 0); //reset timer (feed watchdog)

                if (canBegin)
                {
                    WiFiClient stream = http.getStream();

                    timerWrite(timer_w, 0); //reset timer (feed watchdog)

                    log_message(DEBUG_OTA, "Begin OTA. This may take 2 - 5 mins to complete. Things might be quite for a while.. Patience!");
                    size_t written = Update.writeStream(stream);

                    if (written == contentLength)
                    {
                        log_message(DEBUG_OTA, "Written : " + String(written) + " successfully");
                    }
                    else
                    {
                        log_message(DEBUG_OTA, "Written only : " + String(written) + "/" + String(contentLength) + ". Retry?");
                    }

                    timerWrite(timer_w, 0); //reset timer (feed watchdog)

                    if (Update.end())
                    {
                        log_message(DEBUG_OTA, "OTA done!");

                        timerWrite(timer_w, 0); //reset timer (feed watchdog)

                        if (Update.isFinished())
                        {
                            log_message(DEBUG_OTA, "Update successfully completed. Rebooting.");
                            return true;
                        }
                        else
                        {
                            timerWrite(timer_w, 0); //reset timer (feed watchdog)

                            log_message(DEBUG_OTA, "Update not finished? Something went wrong!");
                            return false;
                        }
                    }
                    else
                    {
                        timerWrite(timer_w, 0); //reset timer (feed watchdog)

                        log_message(DEBUG_OTA, "Error Occurred. Error #: ");
                        return false;
                    }
                }
                else
                {
                    timerWrite(timer_w, 0); //reset timer (feed watchdog)

                    log_message(DEBUG_OTA, "Not enough space to begin OTA");
                    client.flush();
                    return false;
                }
            }
            else
            {
                timerWrite(timer_w, 0); //reset timer (feed watchdog)

                log_message(DEBUG_OTA, "There was no content in the response");
                client.flush();
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}


bool check_for_updates()
    {
        time_t timer_initial = timer_start();
        bool success = false;
        
        led_blinking_ota_check();

        if (wifi_connect_for_ota())
        {
            log_message(DEBUG_OTA, "\n\n ", true);
            log_message(DEBUG_OTA, "WiFi Connected.!");
            log_message(DEBUG_OTA, "Device is checking for firmware updates.");

            String version = String("Current Version - v") + String(CURRENT_VERSION);
            log_message(DEBUG_OTA, version);
            String downloadUrl = getDownloadUrl();
            if (downloadUrl.length() > 0)
            {
            success = downloadUpdate(downloadUrl);
            if (!success)
            {
                log_message(DEBUG_OTA, "Error updating device");
                // return false;
            }
            }
        }
        else
        {
            log_message(DEBUG_OTA, "WiFi not connected");
            led_blinking_ota_check();
            // return false;
        }
        // http.end();
        WiFi.disconnect(true);
        WiFi.mode(WIFI_OFF);
        esp_wifi_stop();

        if (success)
        {
            led_blinking_ota_update_success();
        }
        else
        {
            led_blinking_ota_update_fail();
        }
        

        ota_update_checking_duration = timer_stop(timer_initial);
        return success;
    }